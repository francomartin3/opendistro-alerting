# Open Distro for Elasticsearch Alerting

## Requirements

- Elasticsearch cluster
- Kibana installation
- metricbeat indices
- Slack destination

## What we'll do
- Setup a monitor
- Setup a trigger
- Notify Slack

I'll asume you know your way around opendistro's kibana. You should know how to create visualizations, what aggregations and some basic data types.

## Let's begin
This tasks are the bare minimum to setup alerting, feel free to add more metrics and create richer messages.
### Setup a monitor
First we need to create a query that will bring data so that we can decide if we notify or not.
- To begin, go to Alerting -> Monitors and click con "Create monitor".
- Type a Monitor Name and change the Method of definition to "Define using extraction query" and in the Index dropdown type metricbeat-* and press enter. The dropdown wont let you select a wildcard index, thats why you need to type it.
- Elasticsearch queries are not easy to type, so lets get a query from a visualization. Create a data table visualization using our metricbeat index pattern.
    - Now lets add metrics, ill add the following
        - average of system.load.5 (the first one is the one we will use to determine if we notify or not!)
        - average of system.cpu.total.norm.pct (normalized cpu usage as percentage)
        - average of system.memory.actual.used.pct (memory usage usage as percentage)
    - Lets split our rows by hostname:
        - In the buckets section below metrics click on Add
        - Click on the "split rows" option
        - We will aggregate by the terms in the host.hostname field. In the aggregation dropdown select "Terms" and in the field dropdown select host.hostname. Leave the "order by" field as default.
        - Now click on update. You should see a table with 5 rows with 4 columns. If you dont, check if you mistyped one of the names in the fields.
    - Now that we have our visualization, lets get the extraction query.
        - In the top left click on the "Inspect" option.
        - In the top right click on the View:Data dropdown and select "Requests"
        - Now you should have 3 tabs, "Statistics", "Request" and "Response". Select request.
        - Copy the whole request, you can click on the top right corner of the request section. Theres a copy button. Ill leave another file in this repo named "extraction-query" so you can compare. 
- Paste the request query on the "Define Extraction Query"
- Extraction queries have hardcoded dates, so change the dates to relative dates.
    - Change this: 
        ````
        "range": {
            "@timestamp": {
              "gte": "2020-09-20T17:07:55.414Z",
              "lte": "2020-09-20T17:08:55.414Z",
              "format": "strict_date_optional_time"
            }
          }
        ````
        to this:
        ````
        "range": {
            "@timestamp": {
              "from": "now-5m",
              "to": "now",
              "format": "strict_date_optional_time"
            }
          }
        ````
- On the top right of the "Define monitor" section, there is a "Run" button. Click it and validate you dont have syntax errors. You should see a valid elasticsearch response in the "Extraction query response" section.
- Finally selecte the Monitor Schedule you want. Ill leave it at one minute. Click on Create. Some observations:
    - If you change this, make sure you know what you are doing. The more often you run this the more load you'll be putting on your elasticsearch cluster. 
    - The range of the extraction query, monitor frequency and the trigger condition work together to define if you have issues or not so change values wisely. A poor configuration might not bring enough data to trigger the monitor. 
- Now lets create a trigger. Name the trigger, set a severity level. Ill name it "Load over 1"
- Below the severity, you'll have the response for your extraction query. We are interested in the aggregations section. We have a bucket for each hostname and a key for each metric we added. They are in the order you entered when creating the visualization and the buckets are ordered by the metric you selected when creating the visualization. In my case is load.
- Lets create a trigger condition:
    - To access the response data, add ctx.results[0]. Now you are in the first level of the response. Now add aggregations.4.buckets.0.1.value . The explanation:
        - We want the value of the first metric(load) which has the "1" key, from the first element(0) in the buckets array that is in the key "4" in the "aggregations" keys.
        - An example:
        ````
        {
            "_shards": {
                "total": 28,
                "failed": 0,
                "successful": 28,
                "skipped": 0
            },
            "hits": {
                "hits": [],
                "total": {
                    "value": 1548,
                    "relation": "eq"
                },
                "max_score": null
            },
            "took": 28,
            "timed_out": false,
            "aggregations": {                                       //aggregations.
                "4": {                                              //4.
                    "doc_count_error_upper_bound": 0,
                    "sum_other_doc_count": 967,
                    "buckets": [                                    //buckets.
                        {                                           // 0. (First element in the array, the one with the highest load because I ordered by load.)
                            "1": {                                  // 1.
                                "value": 0.4416666666666667         // value
                            },
                            "2": {
                                "value": 0.10266666666666668
                            },
                            "3": {
                                "value": 0.36116666666666664
                            },
                            "doc_count": 167,
                            "key": "your_hostname"
                        }
                    ]
                }
            }
        }
        ```` 
        The final trigger condition will look like this "ctx.results[0].hits.total.value > 1" This will trigger the actions if our load is greater than 1.
- Test the trigger condition with different values to see if you set everything up correctly.
- Now lets tell kibana what to do if the trigger condition is true. Name the action, select a pre-created destination. For me its slack, if you use something like webhooks, in the message field you'll have to type the whole body of the post request, like {"text":"your message here"}.
- In the message section we will use mustache templates to tell us what the load, cpu usage and memory usage of a server is"
    - Type the following code in the message section:
    ````
    {{#ctx.results.0.aggregations.4.buckets}}
    {{key}}: 
        Load: {{1.value}}
        CPU: {{3.value}}
        Memory: {{4.value}}
    {{/ctx.results.0.aggregations.4.buckets}}
    ````
    - The {{#ctx.results.0.aggregations.4.buckets}} tells mustache to go to the "ctx.results.0.aggregations.4.buckets" array. 
    - The {{key}} tells mustache that for each object in the "ctx.results.0.aggregations.4.buckets" array it should use the "key" key. In my case, "key" has the name of the server.
    - The {{1.value}} tells mustache that for each object in the "ctx.results.0.aggregations.4.buckets" array it should use the "value" key inside the "1" key. In my case, the load is stored as the "value" key inside the "1" key.
- As you change the template, youll see the Message preview change. If you followed correctly, you should the a message like this:
    your_hostname: 
    Load: 0.4416666666666667
    CPU: 0.10266666666666668
    Memory: 0.36116666666666664
- Click on the send test message option to the right. And check your messages. If you dont get the message, check the elasticsearch logs. Perhaps you need a header, or your url is not right or you need to set up a proxy in your elasticsearch servers. 
- You should change the action throttling so you dont get spammed. Click on create.

Now you have a monitor and a trigger. Congratulations!






